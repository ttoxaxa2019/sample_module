
import UIKit
import SnapKit

class LoginView: UIView {
    lazy var emailField: UITextField = {
        let field = UITextField()
        field.backgroundColor = UIColor(hex: 0xEFEFF4)
        field.layer.cornerRadius = 6
        field.font = .systemFont(ofSize: 14)
        field.keyboardType = .emailAddress
        field.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [
            .foregroundColor: UIColor.black
        ])
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 10))
        field.leftView = paddingView
        field.leftViewMode = .always
        field.autocapitalizationType = .none
        return field
    }()

    lazy var passwordField: UITextField = {
        let field = UITextField()
        field.backgroundColor = UIColor(hex: 0xEFEFF4)
        field.layer.cornerRadius = 6
        field.font = .systemFont(ofSize: 14)
        field.attributedPlaceholder = NSAttributedString(string: "Пароль", attributes: [
            .foregroundColor: UIColor.black
        ])
        field.isSecureTextEntry = true
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 10))
        field.leftView = paddingView
        field.leftViewMode = .always
        return field
    }()

    lazy var titleLabel = UILabel(text: "Авторизация", font: .systemFont(ofSize: 19, weight: .bold), textAlignment: .center)
    lazy var errorLabel = UILabel(text: nil, font: .systemFont(ofSize: 13), textColor: .neonRed, textAlignment: .center)

    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .defaultBlueColor
        button.layer.cornerRadius = 6
        button.clipsToBounds = false
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Войти", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14)
        return button
    }()
    
    lazy var recoverButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.defaultBlueColor, for: .normal)
        button.setTitle("Забыл пароль?", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 17)
        return button
    }()

    lazy var registrationLabel: UILabel = {
        let label = UILabel()
        let attachment = NSTextAttachment()
        attachment.image = #imageLiteral(resourceName: "small-arrow-icon")
        let attachmentString = NSAttributedString(attachment: attachment)
        let attributedString = NSMutableAttributedString(string: "Создать новый аккаунт ")
        attributedString.append(attachmentString)
        label.attributedText = attributedString
        label.font = .systemFont(ofSize: 14)
        label.textColor = .defaultBlueColor
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        return label
    }()
    
    lazy var registrationButton: UIButton = {
        let button = UIButton()
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(emailField)
        addSubview(passwordField)
        addSubview(titleLabel)
        addSubview(loginButton)
        addSubview(registrationLabel)
        addSubview(errorLabel)
        addSubview(registrationButton)
        addSubview(recoverButton)

        setupLayout()

        backgroundColor = .white
    }

    func setupLayout() {
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(167)
        }

        emailField.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(30)
            make.leading.trailing.equalToSuperview().inset(38)
            make.height.equalTo(42)
        }

        passwordField.snp.makeConstraints { make in
            make.top.equalTo(emailField.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview().inset(38)
            make.height.equalTo(42)
        }

        loginButton.snp.makeConstraints { make in
            make.top.equalTo(passwordField.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview().inset(38)
            make.height.equalTo(42)
        }

        errorLabel.snp.makeConstraints { make in
            make.top.equalTo(loginButton.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(38)
        }

        registrationLabel.snp.makeConstraints { make in
            make.top.equalTo(loginButton.snp.bottom).offset(119)
            make.leading.trailing.equalToSuperview()
        }
        
        registrationButton.snp.makeConstraints { make in
            make.edges.equalTo(registrationLabel)
        }
        
        recoverButton.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).offset(12)
            make.trailing.equalToSuperview().offset(-15)
            make.height.height.equalTo(20)
            make.width.equalTo(150)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
