
import RxCocoa
import RxSwift

struct LoginInteractor: BusinessLogic {
    let presenter: LoginPresenter
    let service = LoginService()
    
    init(presenter: LoginPresenter) {
        self.presenter = presenter
    }
    
    func transform(request: Driver<Login.Event>) -> Driver<Login.Data> {
        return request.flatMapLatest {
            switch $0 {
            case let .submit(login, password):
                return self.service.auth(with: login, password: password)
                    .map { _ in .successLogin }
                    .asDriver(onErrorJustReturn: .loginFailed)
            case .register:
                return .just(.register)
            case .recover:
                return .just(.recover)
            }
        }
    }
}
