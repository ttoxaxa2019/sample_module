
import RxSwift
import RxCocoa

class LoginPresenter: PresentationLogic {    
    func transform(response: Driver<Login.Data>) -> Driver<Login.State> {
        return response.flatMapLatest {
            switch $0 {
            case .loginFailed:
                return .just(.negative(.auth))
            case .successLogin:
                return .just(.positive(.auth))
            case .register:
                return .just(.positive(.pushRegistration))
            case .recover:
                return .just(.positive(.pushRecover))
            }
        }
    }
}
