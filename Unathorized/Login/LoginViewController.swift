
import UIKit
import RxCocoa
import RxSwift

class LoginViewController: BaseController {
    var state: Driver<Login.State>?
    let interactor: LoginInteractor
    
    init(interactor: LoginInteractor) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func update(with theme: BaseAppearance.Component) { }

    let contentView = LoginView()

    override var hideNavigationBar: Bool {
        return true
    }

    override func loadView() {
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    func setupBindings() {
        state?.drive(onNext: { state in
            switch state {
            case let .positive(values): self.handle(values)
            case let .negative(errors): self.handle(errors)
            default: break
            }
        }).disposed(by: disposeBag)
    }
}

extension LoginViewController: StateRepresentable {
    func collectEvents() -> Driver<Login.Event> {
        let credentials = Observable.combineLatest(
                contentView.emailField.rx.text,
                contentView.passwordField.rx.text,
                resultSelector: { (login: $0, password: $1) }
            )
            .asDriver(onErrorJustReturn: (login: "", password: ""))
        let tapEvent: Driver<Login.Event> = contentView.loginButton.rx.tap
            .asDriver()
            .withLatestFrom(credentials)
            .flatMapLatest { .just(.submit(login: $0.login ?? "", password: $0.password ?? "")) }
        let registerEvent: Driver<Login.Event> = contentView.registrationButton.rx.tap
            .asDriver()
            .flatMapLatest { .just(.register) }
        let recoverEvent: Driver<Login.Event> = contentView.recoverButton.rx.tap
            .asDriver()
            .flatMapLatest { .just(.recover) }
        return .merge(tapEvent, registerEvent, recoverEvent)
    }
    
    func handle(_ errors: Login.Errors) {
        contentView.errorLabel.isHidden = false
        contentView.errorLabel.text = errors.errorDescription
    }
    
    func handle(_ success: Login.Success) {
        contentView.errorLabel.isHidden = true
        
        switch success {
        case .pushRegistration:
            navigationController?.pushViewController(RegistrationAssembly().assemble(), animated: true)
        case .auth:
            let delegate = AppDelegate.shared
            guard let currentRoot = delegate.window?.rootViewController else { return }
            let tabBarController = delegate.configureTabBar()
            UIView.transition(from: currentRoot.view, to: tabBarController.view, duration: 0.3, options: .transitionCrossDissolve, completion: { _ in
                delegate.window?.rootViewController = tabBarController
            })
        case .pushRecover:
            navigationController?.pushViewController(RecoverAssembly().assemble(), animated: true)
        }
    }
}
