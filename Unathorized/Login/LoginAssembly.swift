
class LoginAssembly: Assembly {
    func assemble() -> LoginViewController {
        let presenter = LoginPresenter()
        let interactor = LoginInteractor(presenter: presenter)
        let controller = LoginViewController(interactor: interactor)
        controller.state = presenter.transform(response:
            interactor.transform(request: controller.collectEvents())
        )
        return controller
    }
}
