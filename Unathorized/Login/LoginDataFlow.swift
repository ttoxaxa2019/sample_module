
import RxSwift
import RxCocoa

/// Содержит все сущности являющиеся носителями данных, которыми элементы модуля обмениваются между собой
enum Login {
    typealias State = DisplayState<Success, Errors>

    enum Errors: DisplayError {
        case auth
        
        var errorDescription: String? {
            switch self {
            case .auth:
                return "Логин или пароль введен неверно.\nПопробуйте снова."
            }
        }
    }

    enum Success {
        case auth
        case pushRegistration
        case pushRecover
    }

    enum Event {
        /// Use cases
        case submit(login: String, password: String)
        case register
        case recover
    }

    enum Data {
        /// Use cases
        case successLogin
        case loginFailed
        case register
        case recover
    }
}
