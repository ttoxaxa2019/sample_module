
import RxSwift
import NetworkKit
import RxOptional

struct TokenResponse: Decodable, Serializable {
    let token: String
}

struct User: Decodable, Serializable {
    let email: String
    let firstName: String
    let lastName: String
}

struct LoginService {
    private let client = ApiClientRegister.shared.defaultApiClient()
    func auth(with login: String, password: String) -> Observable<TokenResponse> {
        let parameters: [String: Any] = [
            "email": login,
            "password": password
        ]
        return client.post(endpoint: "auth/obtain/", parameters: parameters).filterNil()
    }
    
    
    func register(login: String, password: String, repeatPassword: String) -> Observable<TokenResponse> {
        let parameters: [String: Any] = [
            "login": login,
            "password": password,
            "repeatPassword": repeatPassword
        ]
        
        let register: Observable<User> = client.post(endpoint: "users/", parameters: parameters).filterNil()
        return register.flatMap { _ in
            self.auth(with: email, password: password)
        }
    }
}
