
enum Registration {
    typealias State = DisplayState<Success, Errors>
    
    struct ViewModel {
        enum Fields {
            case login
            case password
            case repeatPassword
        }
        
        let highlightedFields: [Fields]
        let errorMessage: String?
    }
    
    enum Errors: DisplayError {
        case registration(ViewModel)
        
        var errorDescription: String? {
            switch self {
            case let .registration(viewModel):
                return viewModel.errorMessage
            }
        }
    }
    
    enum Success {
        case registration
        case popToLogin
    }
    
    enum Event {
        /// Use cases
        case submit(login: String, password: String, repeatPassword: String)
        case login
    }
    
    enum Data {
        /// Use cases
        case registration(result: RegistrationResponse)
        case login
    }
    
    enum RegistrationResponse {
        case success
        case validationFailed(error: ValidationService.ServiceError)
        case failed(error: Error)
    }
}

