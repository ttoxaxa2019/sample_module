
import UIKit

class RegistrationView: UIView {
    lazy var titleLabel = UILabel(text: "Регистрация", font: .systemFont(ofSize: 19, weight: .bold), textAlignment: .center)
    lazy var errorLabel = UILabel(text: nil, font: .systemFont(ofSize: 13), textColor: .neonRed, textAlignment: .center)
    
    lazy var emailField: UITextField = {
        let field = UITextField.inputField(with: "Email")
        field.keyboardType = .emailAddress
        return field
    }()
    
    lazy var passwordField: UITextField = {
        let field = UITextField.inputField(with: "Пароль")
        field.isSecureTextEntry = true
        return field
    }()
    
    lazy var repeatField: UITextField = {
        let field = UITextField.inputField(with: "Повторите пароль")
        field.isSecureTextEntry = true
        return field
    }()
    
    lazy var registerButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .defaultBlueColor
        button.layer.cornerRadius = 6
        button.clipsToBounds = false
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Создать аккаунт", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14)
        return button
    }()
    
    lazy var loginLabel: UILabel = {
        let label = UILabel()
        let attachment = NSTextAttachment()
        attachment.image = #imageLiteral(resourceName: "small-arrow-icon")
        let attachmentString = NSAttributedString(attachment: attachment)
        let attributedString = NSMutableAttributedString(string: "Войти используя почту ")
        attributedString.append(attachmentString)
        label.attributedText = attributedString
        label.font = .systemFont(ofSize: 14)
        label.textColor = .defaultBlueColor
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        return label
    }()
    
    lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.spacing = 10
        return view
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton()
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleLabel)
        addSubview(stackView)
        stackView.addArrangedSubview(emailField)
        stackView.addArrangedSubview(passwordField)
        stackView.addArrangedSubview(repeatField)
        stackView.addArrangedSubview(registerButton)
        addSubview(loginLabel)
        addSubview(errorLabel)
        addSubview(loginButton)
        
        setConstraints()
        
        backgroundColor = .white
    }
    
    func setConstraints() {
        loginButton.snp.makeConstraints { make in
            make.edges.equalTo(loginLabel)
        }
        
        emailField.snp.makeConstraints { make in
            make.height.equalTo(42)
        }
        
        passwordField.snp.makeConstraints { make in
            make.height.equalTo(42)
        }
        
        repeatField.snp.makeConstraints { make in
            make.height.equalTo(42)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide).offset(94)
        }
        
        stackView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(30)
            make.leading.trailing.equalToSuperview().inset(38)
            make.height.equalTo(200)
        }
        
        errorLabel.snp.makeConstraints { make in
            make.top.equalTo(stackView.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(38)
        }
        
        loginLabel.snp.makeConstraints { make in
            make.top.equalTo(registerButton.snp.bottom).offset(97)
            make.leading.trailing.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
