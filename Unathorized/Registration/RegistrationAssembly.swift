
import Foundation

struct RegistrationAssembly: Assembly {
    func assemble() -> RegistrationController {
        let presenter = RegistrationPresenter()
        let interactor = RegistrationInteractor(presenter: presenter)
        let controller = RegistrationController(interactor: interactor)
        controller.state = presenter.transform(response:
            interactor.transform(request: controller.collectEvents())
        )
        return controller
    }
}
