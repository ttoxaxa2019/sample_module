
import RxCocoa

class RegistrationPresenter: PresentationLogic {
    func transform(response: Driver<Registration.Data>) -> Driver<Registration.State> {
        return response.flatMapLatest {
            switch $0 {
            case let .registration(result):
                return self.tranform(registrationResult: result)
            case .login:
                return .just(.init(value: .popToLogin))
            }
        }
    }
    
    private func tranform(registrationResult: Registration.RegistrationResponse) -> Driver<Registration.State> {
        switch registrationResult {
        case .failed:
            return .just(.init(value: .registration))
        case let .validationFailed(error):
            if case ValidationService.ServiceError.invalidRepeatPassword = error {
                let viewModel = Registration.ViewModel(
                    highlightedFields: [.repeatPassword],
                    errorMessage: "Пароли не совпадают."
                )
                return .just(.init(error: .registration(viewModel)))
            }
            
            let viewModel = Registration.ViewModel(
                highlightedFields: [],
                errorMessage: "Ошибка. Попробуйте позже."
            )
            return .just(.init(error: .registration(viewModel)))
        case .success:
            return .just(.init(value: .registration))
        }
    }
}
