
import RxSwift
import RxCocoa

class RegistrationInteractor: BusinessLogic {
    let presenter: RegistrationPresenter
    let service = LoginService()
    
    init(presenter: RegistrationPresenter) {
        self.presenter = presenter
    }
    
    func transform(request: Driver<Registration.Event>) -> Driver<Registration.Data> {
        return request.flatMapLatest { event -> Driver<Registration.Data> in
            switch event {
            case let .submit(login, password, repeatPassword):
                return self.service
                    .register(login: login, password: password, repeatPassword: repeatPassword)
                    .map { _ in Registration.Data.registration(result: .success) }
                    .asDriver(onErrorRecover: { error in
                        guard case let SerializationMiddlewareError.error(dictionary) = error else { return .just(.registration(result: .failed(error: error))) }
                        let values = dictionary.values.compactMap { $0 as? String }
                        if values.contains("user with this email address already exists.") {
                            return .just(.registration(result: .validationFailed(error: .invalidEmail)))
                        }
                        
                        return .just(.registration(result: .failed(error: error)))
                    })
            case .login:
                return .just(.login)
            }
        }
    }
}
