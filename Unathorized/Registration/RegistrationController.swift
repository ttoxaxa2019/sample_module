
import Foundation
import RxCocoa
import RxSwift

class RegistrationController: BaseController {
    let contentView = RegistrationView()
    
    var state: Driver<Registration.State>?
    let interactor: RegistrationInteractor
    
    init(interactor: RegistrationInteractor) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = contentView
    }
    
    override func update(with theme: BaseAppearance.Component) { }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    func setupBindings() {
        state?.drive(onNext: { state in
            switch state {
            case let .positive(values): self.handle(values)
            case let .negative(errors): self.handle(errors)
            default: break
            }
        }).disposed(by: disposeBag)
    }
}

extension RegistrationController: StateRepresentable {
    func collectEvents() -> Driver<Registration.Event> {
        let credentials = Observable.combineLatest(
            [contentView.emailField.rx.text, contentView.passwordField.rx.text, contentView.repeatField.rx.text],
            { result in
                (login: result[0], password: result[1], repeatPassword: result[2])
            })
            .asDriver(onErrorJustReturn: (login: nil, password: nil, repeatPassword: nil))
        let tapEvent: Driver<Registration.Event> = contentView.registerButton.rx.tap
            .asDriver()
            .withLatestFrom(credentials)
            .flatMapLatest {
                .just(.submit(
                    login: $0.login ?? "",
                    password: $0.password ?? "",
                    repeatPassword: $0.repeatPassword ?? "")
                )
            }
        let loginEvent: Driver<Registration.Event> = contentView.loginButton.rx.tap
            .asDriver()
            .flatMapLatest { .just(.login) }
        return .merge(tapEvent, loginEvent)
    }
    
    func handle(_ errors: Registration.Errors) {
        contentView.errorLabel.isHidden = false
        
        switch errors {
        case let .registration(viewModel):
            contentView.errorLabel.text = viewModel.errorMessage
            highlightFields(fields: viewModel.highlightedFields)
        }
    }
    
    private func highlightFields(fields: [Registration.ViewModel.Fields]) {
        fields.forEach {
            let fieldToHighlight: UIView
            switch $0 {
            case .login:
                fieldToHighlight = contentView.emailField
            case .password:
                fieldToHighlight = contentView.passwordField
            case .repeatPassword:
                fieldToHighlight = contentView.repeatField
            }
            fieldToHighlight.backgroundColor = .palePink
        }
    }
    
    func handle(_ success: Registration.Success) {
        contentView.errorLabel.isHidden = true
        contentView.emailField.backgroundColor = .paleGrey
        contentView.passwordField.backgroundColor = .paleGrey
        contentView.repeatField.backgroundColor = .paleGrey
        
        switch success {
        case .registration:
            break
        case .popToLogin:
            navigationController?.popViewController(animated: true)
        }
    }
}

